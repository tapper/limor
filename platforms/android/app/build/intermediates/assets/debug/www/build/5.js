webpackJsonp([5],{

/***/ 815:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(825);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 825:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_popups_popups__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_api__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config_config_app__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, Popup, auth, navParams, alertCtrl, storage, mixpanel, api, events, config) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.Popup = Popup;
        this.auth = auth;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.mixpanel = mixpanel;
        this.api = api;
        this.events = events;
        this.config = config;
        this.Login = {
            "mail": "",
            "password": ""
        };
        this.mixpanel.init(api.mixPanel).then(function () {
            console.log("Mix : ");
            _this.mixpanel.track("LogInPage : ", {});
        }).catch(function (e) { return console.log('Error MixPanel', e); });
    }
    LoginPage.prototype.loginUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data_1, goToBasket;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //!this.emailregex.test(this.Login.mail)
                        this.emailregex = /\S+@\S+\.\S+/;
                        console.log("Login1");
                        if (!(this.Login.mail == "")) return [3 /*break*/, 1];
                        this.Popup.presentAlert("Name Error", 'Please enter User name', '');
                        return [3 /*break*/, 7];
                    case 1:
                        if (!(this.Login.mail.length < 3)) return [3 /*break*/, 2];
                        this.Popup.presentAlert("Name Error", 'Please Enter correct name', '');
                        this.Login.mail = '';
                        console.log("Login2");
                        return [3 /*break*/, 7];
                    case 2:
                        if (!(this.Login.password == "")) return [3 /*break*/, 3];
                        console.log("Login3");
                        this.Popup.presentAlert("Password Error", 'Please enter password', '');
                        return [3 /*break*/, 7];
                    case 3:
                        console.log("Login4 ", this.Login);
                        return [4 /*yield*/, this.auth.LoginUser("UserLogin", this.Login, this.config.push_id)];
                    case 4:
                        data_1 = _a.sent();
                        this.mixpanel.init(this.api.mixPanel).then(function () {
                            console.log("Mix : ");
                            _this.mixpanel.track("AfterLogInPage : " + data_1[0].name, {}).catch(function (e) { return console.log('Error MixPanel', e); });
                            ;
                        });
                        console.log("login response: ", data_1);
                        if (!(data_1 == 0)) return [3 /*break*/, 5];
                        this.Popup.presentAlert("Password Error", 'Your name and password not match please try again', '');
                        this.Login.password = '';
                        return [3 /*break*/, 7];
                    case 5:
                        this.storage.set('userid', data_1[0].id);
                        this.storage.set('name', data_1[0].name);
                        this.storage.set('is_Admin', data_1[0].is_Admin);
                        localStorage.setItem('userid', data_1[0].id);
                        localStorage.setItem('name', data_1[0].name);
                        localStorage.setItem('is_Admin', data_1[0].is_Admin);
                        this.events.publish('userConnected', data_1[0].is_Admin);
                        this.Login.mail = '';
                        this.Login.password = '';
                        return [4 /*yield*/, this.api.getCategories("GetCategories")];
                    case 6:
                        _a.sent();
                        goToBasket = "0";
                        try {
                            goToBasket = localStorage.basket;
                        }
                        catch (err) {
                        }
                        if (goToBasket == "1") {
                            localStorage.basket = "0";
                            this.navCtrl.push('BasketPage');
                        }
                        else
                            this.navCtrl.push('HomePage');
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.goRegisterPage = function () {
        this.navCtrl.setRoot('RegisterPage');
    };
    LoginPage.prototype.goForgotPage = function () {
        //this.navCtrl.push(ForgotPage);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"G:\gitlab\limor\src\pages\login\login.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <header></header>\n</ion-header>\n\n\n\n<ion-content padding>\n    <ion-item no-lines class="RegTitle">\n        <label>Login </label>\n    </ion-item>\n\n    <ion-list class="Inputs">\n\n        <ion-item>\n            <ion-label floating>Please enter UserName</ion-label>\n            <ion-input type="text" [(ngModel)]="Login.mail" name="mail" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>please enter password</ion-label>\n            <ion-input type="password" [(ngModel)]="Login.password" name="password" ></ion-input>\n        </ion-item>\n\n\n        <div style="width: 100%; margin-top: 20px;" align="center" (click)="loginUser()">\n            <div style="width: 90%">\n                <button ion-button color="primary" style="width: 100%">Login</button>\n            </div>\n        </div>\n\n\n        <div style="width: 100%; margin-top: 2px;" align="center" (click)="goRegisterPage()">\n            <div style="width: 90%">\n                <button ion-button color="primary" style="width: 100%">go to register</button>\n            </div>\n        </div>\n\n        <!-- <div style="width: 100%; margin-top: 2px;" align="center" (click)="goRegisterPage()">\n            <div style="width: 90%">\n                <button ion-button color="primary"  style="width: 100%; background-color: #3B5998; height: 50px;">Facebook Connect</button>\n            </div>\n        </div> -->\n\n        <!-- <facebookconnect></facebookconnect> -->\n\n        <!--<div (click)="goForgotPage()">-->\n            <!--<p>forgot password</p>-->\n        <!--</div>-->\n\n\n\n    </ion-list>\n</ion-content>\n<ion-footer class="FooterClass">\n    <footer></footer>\n</ion-footer>'/*ion-inline-end:"G:\gitlab\limor\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_popups_popups__["a" /* PopupsProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__["a" /* Mixpanel */],
            __WEBPACK_IMPORTED_MODULE_5__providers_api_api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_6__providers_config_config_app__["a" /* ConfigApp */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=5.js.map