webpackJsonp([3],{

/***/ 820:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__products__ = __webpack_require__(830);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(416);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductsPageModule = /** @class */ (function () {
    function ProductsPageModule() {
    }
    ProductsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__products__["a" /* ProductsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__products__["a" /* ProductsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], ProductsPageModule);
    return ProductsPageModule;
}());

//# sourceMappingURL=products.module.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config_app__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_mixpanel__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProductsPage = /** @class */ (function () {
    function ProductsPage(navCtrl, mixpanel, zone, api, config, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.mixpanel = mixpanel;
        this.zone = zone;
        this.api = api;
        this.config = config;
        this.navParams = navParams;
        this.subCatgories = this.api.subCategories;
        this.products = this.api.products;
        this.Host = this.config.ServerImageHost;
        this.subCategoryName = "";
        this.selectedCategory = 0;
        console.log(this.subCatgories);
        this.subCategoryName = this.subCatgories[0].title;
        this.selectedCategory = this.subCatgories[0]['id'];
        console.log("subCategoryName : ", this.subCategoryName, this.selectedCategory, this.subCatgories);
        //suscribe for product
        this.api._products.subscribe(function (val) {
            _this.zone.run(function () {
                _this.products = val;
                console.log("PRODUCTS : ", _this.products);
            });
        });
        this.mixpanel.init(api.mixPanel).then(function () {
            console.log("Mix : ", _this.subCategoryName);
            mixpanel.track("Products : ", {
                "SubCategory": _this.subCategoryName,
            });
        }).catch(function (e) { return console.log('Error MixPanel', e); });
    }
    ProductsPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    ProductsPage.prototype.subCategoryClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data, obj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("SubClick");
                        return [4 /*yield*/, this.api.getProducts("getProducts", this.selectedCategory)];
                    case 1:
                        data = _a.sent();
                        console.log("MyData : ", data);
                        obj = this.subCatgories.findIndex(function (item) { return item['id'] == _this.selectedCategory; });
                        this.subCategoryName = obj['title']; //this.subCatgories[i].title;
                        console.log("ProductsPage : ", this.products);
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductsPage.prototype.goLeft = function () {
    };
    ProductsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-products',template:/*ion-inline-start:"G:\gitlab\limor\src\pages\products\products.html"*/'<!--\n\n  Generated template for the ProductsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!--<ion-header>-->\n\n    <!--<header-comp></header-comp>-->\n\n<!--</ion-header>-->\n\n<ion-header>\n\n    <header-comp [name]="subCategoryName"></header-comp>\n\n\n\n<!-- <div>\n\n    <div class="arrowLeft">\n\n        <img src="images/left.png" width="100%"/>\n\n    </div>\n\n    <div class="arrowRight">\n\n            <img src="images/right.png" width="100%"/>\n\n    </div>\n\n    <div class="scrollmenu">\n\n        <span *ngFor="let sub of subCatgories let i = index" (click)="subCategoryClick(sub.id,i)">{{sub.title}}</span>\n\n    </div>\n\n</div> -->\n\n\n\n\n\n    <ion-item>\n\n        <ion-label>Select category</ion-label>\n\n        <ion-select [(ngModel)]="selectedCategory" (ngModelChange)="subCategoryClick($event)">\n\n          <ion-option *ngFor="let sub of subCatgories let i = index" value="{{sub.id}}" >{{sub.title}}</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding class="mainContent">\n\n    <products [productsArray]="products"></products>\n\n</ion-content>\n\n\n\n\n\n<ion-footer>\n\n    <footer></footer>\n\n</ion-footer>\n\n'/*ion-inline-end:"G:\gitlab\limor\src\pages\products\products.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_mixpanel__["a" /* Mixpanel */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */], __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_config_config_app__["a" /* ConfigApp */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */]])
    ], ProductsPage);
    return ProductsPage;
}());

//# sourceMappingURL=products.js.map

/***/ })

});
//# sourceMappingURL=3.js.map