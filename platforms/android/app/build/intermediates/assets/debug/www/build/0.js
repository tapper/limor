webpackJsonp([0],{

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TakanonPageModule", function() { return TakanonPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__takanon__ = __webpack_require__(826);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(416);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TakanonPageModule = /** @class */ (function () {
    function TakanonPageModule() {
    }
    TakanonPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__takanon__["a" /* TakanonPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__takanon__["a" /* TakanonPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], TakanonPageModule);
    return TakanonPageModule;
}());

//# sourceMappingURL=takanon.module.js.map

/***/ }),

/***/ 826:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TakanonPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TakanonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TakanonPage = /** @class */ (function () {
    function TakanonPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TakanonPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TakanonPage');
    };
    TakanonPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-takanon',template:/*ion-inline-start:"G:\gitlab\limor\src\pages\takanon\takanon.html"*/'<!--\n  Generated template for the TakanonPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <header-comp name="Home Page"></header-comp>\n</ion-header>\n\n\n<ion-content padding>\n  <div class="takanon">\n      For purposes of convenience, the Code of Ethics applies to the male, but all that is said therein refers to women and men alike.<br><br>\n      The use of the site and its contents and services are offered to you subject to your acceptance of all the conditions contained in these regulations.<br><br>\n      Browsing the Site and / or subscribing to its services will be considered as your agreement to these terms.<br><br>\n      The site\'s management may suspend, block or stop the surfer\'s access to the service immediately if he violates the terms of the regulations.<br><br>\n      The site\'s management may update the terms of the articles from time to time.<br><br>\n      No use may be made of the site or through it for illegal purposes.<br><br>\n      Registration on the Site is for the personal and exclusive use of the surfer who is not authorized to transfer the user\'s permission to any other person. A special obligation to be absolutely precise in all the personal details required for registration and for the ongoing contact with the subscriber.<br><br>\n      You are solely responsible for the accuracy of the information you publish or transmit through the Site. The site management does not assume any responsibility for the content published or transmitted among subscribers.<br><br>\n      No commercial use may be made of the site by way of advertisement delivery or otherwise.<br><br>\n      The site\'s management intends that the site and its services will be available at all times. At the same time, the site management is unable to commit itself to continuous availability without any mishaps and without "falls". In addition, the site\'s management is entitled to terminate the use of the site from time to time for its maintenance and organization. No monetary compensation will be granted due to malfunctions or breaks in service.<br><br>\n      All intellectual property rights and copyrights relating to the Site are the property of the Site\'s management.<br><br>\n      Site management is not responsible for the content of ads, "banners" or any promotional material on the site. Advertisers are solely responsible for this.<br><br>\n      The existence of links ("links") to other sites does not guarantee the contents of these sites in terms of their reliability, completeness, or in any other way.<br><br>\n      Israeli law will apply to the articles of association. Jurisdiction over it to the competent courts in Tel Aviv.<br><br>\n      Shipments to any place in the country between 7 and 10 working days<br><br>\n      Product return option can be returned up to 15 working days<br><br>\n      Guaranteed refund (shipping back to buyer\'s account)<br><br>\n      We guarantee that all products sold on the site are genuine   <br><br>   \n  </div>\n</ion-content>\n'/*ion-inline-end:"G:\gitlab\limor\src\pages\takanon\takanon.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */]])
    ], TakanonPage);
    return TakanonPage;
}());

//# sourceMappingURL=takanon.js.map

/***/ })

});
//# sourceMappingURL=0.js.map