webpackJsonp([1],{

/***/ 820:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(830);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_popups_popups__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config_config_app__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, alertCtrl, modalCtrl, Popup, auth, api, mixpanel, storage, config) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.Popup = Popup;
        this.auth = auth;
        this.api = api;
        this.mixpanel = mixpanel;
        this.storage = storage;
        this.config = config;
        this.isAvaileble = false;
        this.TotalPrice = 0;
        this.Details = {
            'name': '',
            'mail': '',
            'password': '',
            'address': '',
            'phone': '',
            'newsletter': false
        };
        console.log("Reg");
        mixpanel.init(api.mixPanel).then(function () {
            console.log("Mix : ");
            mixpanel.track("RegisterPage : ", {});
        }).catch(function (e) { return console.log('Error MixPanel', e); });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
    };
    RegisterPage.prototype.ngOnInit = function () {
    };
    RegisterPage.prototype.gotoLogIn = function () {
        console.log("tt23");
        this.navCtrl.push('LoginPage');
    };
    RegisterPage.prototype.doRegister = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data_1, goToBasket;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.emailregex = /\S+@\S+\.\S+/;
                        if (!(this.Details.name.length < 3)) return [3 /*break*/, 1];
                        this.Popup.presentAlert("Name Error", 'Please enter name', '');
                        return [3 /*break*/, 9];
                    case 1:
                        if (!(this.Details.address.length < 3)) return [3 /*break*/, 2];
                        this.Popup.presentAlert("Address Error", 'Please enter address', '');
                        return [3 /*break*/, 9];
                    case 2:
                        if (!(this.Details.mail == "")) return [3 /*break*/, 3];
                        this.Popup.presentAlert("Name Error", 'Please enter name', '');
                        return [3 /*break*/, 9];
                    case 3:
                        if (!(this.Details.mail.length < 3)) return [3 /*break*/, 4];
                        this.Popup.presentAlert("Name Error", 'Please Enter correct name', '');
                        this.Details.mail = '';
                        return [3 /*break*/, 9];
                    case 4:
                        if (!(this.Details.password == "")) return [3 /*break*/, 5];
                        this.Popup.presentAlert("Password Error", 'Please enter password', '');
                        return [3 /*break*/, 9];
                    case 5: return [4 /*yield*/, this.auth.RegisterUser("RegisterUser", this.Details, this.config.push_id)];
                    case 6:
                        data_1 = _a.sent();
                        console.log("register response: ", data_1);
                        if (!(data_1 != 0)) return [3 /*break*/, 8];
                        this.mixpanel.init(this.api.mixPanel).then(function () {
                            console.log("Mix : ");
                            _this.mixpanel.track("AfterRegisterInPage : " + data_1[0].name, {}).catch(function (e) { return console.log('Error MixPanel', e); });
                            ;
                        }).catch(function (e) { return console.log('Error MixPanel', e); });
                        this.storage.set('userid', data_1[0].id);
                        this.storage.set('name', data_1[0].name);
                        this.storage.set('is_Admin', 0);
                        localStorage.setItem('userid', data_1[0].id);
                        localStorage.setItem('name', data_1[0].name);
                        localStorage.setItem('is_Admin', '0');
                        this.Details.name = '';
                        this.Details.mail = '';
                        this.Details.address = '';
                        this.Details.phone = '';
                        this.Details.newsletter = false;
                        return [4 /*yield*/, this.api.getCategories("GetCategories")];
                    case 7:
                        _a.sent();
                        goToBasket = "0";
                        try {
                            goToBasket = localStorage.basket;
                        }
                        catch (err) {
                        }
                        if (goToBasket == "1") {
                            localStorage.basket = "0";
                            this.navCtrl.push('BasketPage');
                        }
                        else
                            this.navCtrl.push('HomePage');
                        return [3 /*break*/, 9];
                    case 8:
                        this.Popup.presentAlert("Details Error", 'This userName already exists , please enter another mail', '');
                        _a.label = 9;
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/home/galp/web-project/limor/limor/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <header></header>\n</ion-header>\n\n\n\n<ion-content padding>\n    <ion-item no-lines class="RegTitle">\n        <label>Limor</label>\n    </ion-item>\n\n    <ion-list class="Inputs">\n        <ion-item>\n            <ion-label floating>Full Name</ion-label>\n            <ion-input type="text" [(ngModel)]="Details.name" name="name" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Address for delivery</ion-label>\n            <ion-input type="text" [(ngModel)]="Details.address" name="address" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>UserName</ion-label>\n            <ion-input type="text" [(ngModel)]="Details.mail" name="mail" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Password</ion-label>\n            <ion-input type="text" [(ngModel)]="Details.password" name="password" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Phone</ion-label>\n            <ion-input type="tel" [(ngModel)]="Details.phone" name="phone" ></ion-input>\n        </ion-item>\n\n        <!--<ion-item style="margin-top: 20px;" no-lines>-->\n            <!--<ion-label>New</ion-label>-->\n            <!--<ion-checkbox color="dark" [(ngModel)]="Details.newsletter" ></ion-checkbox>-->\n        <!--</ion-item>-->\n\n        <div style="width: 100%; margin-top: 20px;" align="center">\n            <div style="width: 90%" (click)="doRegister()">\n                <button ion-button color="primary" style="width: 100%">REGISTER</button>\n            </div>\n\n            <div class="loginButton" (click)="gotoLogIn()" >\n                <p>already registered ? \n                </p>\n            </div>\n        </div>\n    </ion-list>\n</ion-content>\n<ion-footer class="FooterClass">\n    <footer></footer>\n</ion-footer>'/*ion-inline-end:"/home/galp/web-project/limor/limor/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_popups_popups__["a" /* PopupsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__["a" /* Mixpanel */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__providers_config_config_app__["a" /* ConfigApp */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=1.js.map