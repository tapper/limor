webpackJsonp([8],{

/***/ 810:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasketPageModule", function() { return BasketPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basket__ = __webpack_require__(823);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(416);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BasketPageModule = /** @class */ (function () {
    function BasketPageModule() {
    }
    BasketPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__basket__["a" /* BasketPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__basket__["a" /* BasketPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], BasketPageModule);
    return BasketPageModule;
}());

//# sourceMappingURL=basket.module.js.map

/***/ }),

/***/ 823:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api_basket__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config_app__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_delete_popup_delete_popup__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the BasketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BasketPage = /** @class */ (function () {
    function BasketPage(navCtrl, navParams, basketProvider, zone, config, modalCtrl, storage, alertCtrl, api, mixpanel) {
        // this.basketProvider._basket.subscribe(val => {
        //     this.zone.run(() => {
        //         this.basket = val;
        //         console.log(this.basket)
        //     });
        // });
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.basketProvider = basketProvider;
        this.zone = zone;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.api = api;
        this.mixpanel = mixpanel;
        this.basket = this.basketProvider.basket;
        this.Host = this.config.ServerImageHost;
        this.userId = localStorage.getItem('userid');
        this.AppSettings = [];
        mixpanel.init(api.mixPanel).then(function () {
            console.log("Mix : ");
            mixpanel.track("Basket : ", {});
        }).catch(function (e) { return console.log('Error MixPanel', e); });
        this.basketProvider.basket$.subscribe(function (basket) { return _this.basket = basket; });
        this.basketProvider.TotalBasketPrice$.subscribe(function (TotalBasketPrice) { return _this.TotalBasketPrice = TotalBasketPrice; });
        this.basketProvider.calculateTotalprice();
        this.AppSettings = this.config.AppSettings;
        console.log(this.basket);
    }
    BasketPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('ionViewDidLoad BasketPage');
                return [2 /*return*/];
            });
        });
    };
    BasketPage.prototype.changeQuan = function (i, type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.basketProvider.changeQnt(i, type);
                return [2 /*return*/];
            });
        });
    };
    BasketPage.prototype.openDeleteModal = function (i) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var modal;
            return __generator(this, function (_a) {
                this.DeleteId = i;
                modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_delete_popup_delete_popup__["a" /* DeletePopupComponent */]);
                modal.present();
                modal.onDidDismiss(function (data) {
                    if (data == 1) {
                        _this.basketProvider.deleteProduct(_this.DeleteId);
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    BasketPage.prototype.sendBasket = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var basketEmpty, newAlert;
            return __generator(this, function (_a) {
                if (this.basket.length == 0) {
                    basketEmpty = this.alertCtrl.create({
                        title: 'Your shopping cart is empty',
                        buttons: ['OK']
                    });
                    basketEmpty.present();
                }
                else {
                    if (this.userId != "" && this.userId != undefined && this.userId != null) {
                        newAlert = this.alertCtrl.create({
                            title: this.AppSettings.basket_order_text,
                            //subTitle: '',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.basketProvider.sendBasket('getBasket');
                                        _this.basketProvider.clearBasket();
                                        _this.storage.set('basket', '');
                                        _this.navCtrl.setRoot('HomePage');
                                    }
                                }
                            ]
                        });
                        newAlert.present();
                    }
                    else {
                        localStorage.basket = "1";
                        this.navCtrl.push('RegisterPage');
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    BasketPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-basket',template:/*ion-inline-start:"/home/galp/web-project/limor/limor/src/pages/basket/basket.html"*/'<!--\n  Generated template for the BasketPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <header-comp name="Basket"> </header-comp>\n</ion-header>\n\n\n\n\n<ion-content class="Content">\n    <div *ngFor="let item of basket let i=index" >\n        <div class="itemRow">\n\n            <div class="infoCheck" (click)="openDeleteModal(i)">\n                <img src="images/uncheck.png" style="width:100%">\n            </div>\n\n\n            <div class="infoButton">\n                <div class="infoButtonDvLeft"(click)="changeQuan(i,0)">-</div>\n                <div class="infoButtonDv" >\n                    <input type="tel" class="qntText" [(ngModel)] = "item.qnt">\n                </div>\n                <div class="infoButtonDvRight"  (click)="changeQuan(i,1)">+</div>\n            </div>\n\n\n            <div class="infoImage" align="center">\n                <img src="{{Host}}{{item.image}}"  />\n            </div>\n\n            <div class="infoCombo">\n                <div class="title">{{item.title}}</div>\n                <div class="price">price : {{item.low_price}} <span>NIS</span></div>\n                <div class="qnt">qnt : {{item.qnt}}</div>\n            </div>\n\n\n            <!--<div class="infoCheck">-->\n                <!--<img src="images/uncheck.png" *ngIf="item.isClicked == \'0\'" ng-click="changeCheckBox($index,item,0)" style="width:100%">-->\n                <!--<img src="images/checked.png" *ngIf="item.isClicked == \'1\'" ng-click="changeCheckBox($index,item,1)" style="width:100%">-->\n            <!--</div>-->\n            <div>\n                <img  src="images/line1.png" style="width:100%; margin-top:10px;" />\n            </div>\n        </div>\n    </div>\n</ion-content>\n\n<ion-footer>\n\n\n    <div footerContent>\n        <div class="footerRight">\n            {{TotalBasketPrice}} <span>NIS</span>\n        </div>\n        <div class="footerLeft" (click)="sendBasket()">\n            ORDER\n        </div>\n    </div>\n\n\n    <footer></footer>\n\n\n</ion-footer>\n'/*ion-inline-end:"/home/galp/web-project/limor/limor/src/pages/basket/basket.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api_basket__["a" /* BasketProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config_config_app__["a" /* ConfigApp */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__["a" /* Mixpanel */]])
    ], BasketPage);
    return BasketPage;
}());

//# sourceMappingURL=basket.js.map

/***/ })

});
//# sourceMappingURL=8.js.map