webpackJsonp([4],{

/***/ 817:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product__ = __webpack_require__(827);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(416);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductPageModule = /** @class */ (function () {
    function ProductPageModule() {
    }
    ProductPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product__["a" /* ProductPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product__["a" /* ProductPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], ProductPageModule);
    return ProductPageModule;
}());

//# sourceMappingURL=product.module.js.map

/***/ }),

/***/ 827:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_go_to_basket_popup_go_to_basket_popup__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api_basket__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_config_config_app__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProductPage = /** @class */ (function () {
    function ProductPage(navCtrl, navParams, modalCtrl, mixpanel, basketProvider, config, api) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.mixpanel = mixpanel;
        this.basketProvider = basketProvider;
        this.config = config;
        this.api = api;
        this.baskets = [];
        this.categoriesProducts = [];
        this.productId = '';
        this.product = navParams.get('product');
        this.productId = this.product.id;
        this.PrdQuan = 1;
        this.is_Admin = localStorage.getItem('is_Admin');
        this.Host = this.config.ServerImageHost;
        console.log("product:", this.product);
        this.basketProvider.basket$.subscribe(function (basket) { return _this.baskets = basket; });
        this.getSubCategoryProducts(this.product.id, this.product.sub_category_id);
        console.log("Product Page : ");
        this.mixpanel.init(api.mixPanel).then(function () {
            console.log("Mix : ", _this.product);
            mixpanel.track("Product : ", {
                "Product Name": _this.product['title'],
                "Category Name": _this.product['CategoryName'],
                "SKU": _this.product['sku'],
                "id": _this.product['id']
            });
        })
            .catch(function (e) { return console.log('Error MixPanel', e); });
    }
    ProductPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductPage');
    };
    ProductPage.prototype.changeQuan = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                type === 1 ? this.PrdQuan++ : this.PrdQuan--;
                if (this.PrdQuan < 1)
                    this.PrdQuan = 1;
                return [2 /*return*/];
            });
        });
    };
    ProductPage.prototype.openModal = function () {
        var _this = this;
        console.log("Modal");
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_go_to_basket_popup_go_to_basket_popup__["a" /* GoToBasketPopupComponent */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log("bs3 : ", _this.product);
            if (data) {
                _this.product.qnt = _this.PrdQuan;
                _this.basketProvider.pushToBasket(_this.product);
                data == 1 ? _this.navCtrl.pop() : _this.navCtrl.push('BasketPage');
            }
        });
    };
    ProductPage.prototype.gotoChatPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* ChatPage */], { product_id: this.product.id });
    };
    ProductPage.prototype.getSubCategoryProducts = function (prd_id, subcat_id) {
        var _this = this;
        this.api.getSubCategoryProducts('getSubCategoryProducts', prd_id, subcat_id).then(function (data) {
            _this.categoriesProducts = data.json();
            console.log("categoriesProducts : ", _this.categoriesProducts, prd_id, subcat_id);
        });
    };
    ProductPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-product',template:/*ion-inline-start:"/home/galp/web-project/limor/limor/src/pages/product/product.html"*/'<!--\n  Generated template for the ProductPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>-->\n\n  <!--<ion-navbar>-->\n    <!--<ion-title>{{product.title}}</ion-title>-->\n  <!--</ion-navbar>-->\n\n<!--</ion-header>-->\n\n\n<ion-header>\n    <header-comp name="product"> </header-comp>\n</ion-header>\n\n<!--<ion-header>-->\n    <!--<ion-navbar no-border-top class="ToolBarClass">-->\n\n        <!--<ion-title>-->\n            <!--<div style="width: 100%">-->\n                <!--<ion-title class="mainTitle">Limor</ion-title>-->\n            <!--</div>-->\n        <!--</ion-title>-->\n        <!---->\n    <!--</ion-navbar>-->\n<!--</ion-header>-->\n\n\n<ion-content>\n\n    <div align="center" class="imageDiv" *ngIf="!product.product_images">\n        <img src="{{product.FullPath}}" class="mainImage" />\n    </div>\n\n    <div align="center" class="imageDiv" *ngIf="product.product_images">\n        <ion-slides style="height: auto; margin-top: 0px;" *ngIf="product.product_images" autoplay="3000" loop="true" pager="true">\n\n            <ion-slide   >\n                <img src="{{product.FullPath}}" class="mainImage" />\n            </ion-slide>\n            <ion-slide   *ngFor="let slide of product.product_images">\n                <img src="{{Host+slide.url}}" />\n            </ion-slide>\n        </ion-slides>\n    </div>\n    <div align="center" class="info">\n        <p class="title">{{product.title}}</p>\n        <!--<p class="description">{{product.description}}</p>-->\n        <p class="description" [innerHTML]="product.description"></p>\n\n        <h3 class="price" >price :  &#8362; {{product.low_price}}\n            <s *ngIf="product.high_price > 0"> &#8362; {{product.high_price}} </s>\n        </h3>\n    </div>\n\n    <div class="detailsLine"></div>\n        <h5 class="qnt"> Qnt :  </h5>\n        <div class="infoButton">\n            <div class="infoButtonDvLeft"(click)="changeQuan(0)">-</div>\n            <div class="infoButtonDv" >\n                <input type="number" class="qntText" [(ngModel)] = "PrdQuan">\n            </div>\n            <div class="infoButtonDv" style="padding-left:15px; padding-right:15px; width:55px; text-align:center; background-color:#f2f2f2" (click)="changeQuan(1)">+</div>\n        </div>\n        <div class="detailsLine"></div>\n        <h5 class="qnt"> Delivery Details :  </h5>\n        <textarea class="detailsInfo" rows="4" cols="50" ng-model="product.remarks" style="color:#999"></textarea>\n\n\n\n    <products [productsArray]="categoriesProducts"></products>\n\n\n</ion-content>\n\n\n<ion-footer>\n\n    <div class="footerContent">\n        <div class="productFooter" (click)="openModal()" >\n            <span class="productFooterText" >Add to basket >></span>\n        </div>\n    </div>\n\n    <footer [ProductId]="productId"></footer>\n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"/home/galp/web-project/limor/limor/src/pages/product/product.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_mixpanel__["a" /* Mixpanel */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api_basket__["a" /* BasketProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_config_config_app__["a" /* ConfigApp */], __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */]])
    ], ProductPage);
    return ProductPage;
}());

//# sourceMappingURL=product.js.map

/***/ })

});
//# sourceMappingURL=4.js.map