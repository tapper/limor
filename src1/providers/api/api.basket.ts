
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Product} from './api.product';
import {Subject} from "rxjs/Subject";
import {LoadingController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Http} from "@angular/http";
import {ConfigApp} from "../config/config.app";



@Injectable()
export class BasketProvider {

    //public basket:any[] = [];
    // public _basket = new Subject<any>();


    public _basket: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>([]);
    basket$: Observable<any> = this._basket.asObservable();
    get basket(): Array<any> {return this._basket.getValue();}
    
    public _TotalBasketPrice: BehaviorSubject<any> = new BehaviorSubject(null);
    TotalBasketPrice$: Observable<any> = this._TotalBasketPrice.asObservable();
    get TotalBasketPrice(): Array<any> {return this._TotalBasketPrice.getValue();}
    
    
    constructor(public loadingCtrl: LoadingController,
                public storage: Storage,
                private http:Http,
                private config:ConfigApp) {}
    
    
    pushToBasket(Obj)
    {
        let basket = this.basket;
        let len = basket ? basket.length : 0;
        console.log("Len : " , len)
        if(len>0)
             basket.splice(len,0,Obj);
        else
            basket.push(Obj)

        this._basket.next(basket);
        this.storage.set('basket', JSON.stringify(basket));
        this.calculateTotalprice();
        console.log("Basket : " , basket);
    }

    addLocalStorageToBasket(json)
    {
        this._basket.next(json);
    }
    
    deleteProduct(i)
    {
        this.basket.splice(i,1);
        this.storage.set('basket', JSON.stringify(this.basket));
        this.calculateTotalprice();
    }
    
    changeQnt(i,type)
    {
        type === 1 ? this.basket[i].qnt++ : this.basket[i].qnt--;
        if(this.basket[i].qnt < 1 ) this.basket[i].qnt = 1 ;
        this.storage.set('basket', JSON.stringify(this.basket));
        this.calculateTotalprice();
    }
    
    calculateTotalprice() {
        let Total = 0;
        
        if(this.basket)
        {
            this.basket.forEach(item => {
                Total+= (item.low_price*item.qnt);
            });
    
            this._TotalBasketPrice.next(Total);
        }
    }
    
    async sendBasket(url)
    {
        console.log("MyBasket : " , this.basket)
        let body = new FormData();
        body.append('basket',JSON.stringify(this.basket))
        body.append('sum',this.TotalBasketPrice.toString())
        
        body.append('userid', localStorage.getItem('userid'));
        let Basket = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response);
    }

    clearBasket()
    {
        this._basket.next([]);
        //let basket = this.basket;
        //basket = [];
    }
}