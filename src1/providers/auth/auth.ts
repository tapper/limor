import { Injectable } from '@angular/core';
import {Product} from "../api/api.product";
import {Restangular} from 'ngx-restangular';
import {LoadingController} from "ionic-angular";
import {Http} from "@angular/http";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
    
    public ServerUrl = "http://limor.tapper.org.il/laravel/public/api/";
    
    
  constructor(public restangular: Restangular,
              public loadingCtrl: LoadingController,
              public http:Http,) {
  }
  
    // Get all categories ;
    async LoginUser(url,params,push_id)
    {
        return new Promise(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            
            try {
                let body = new FormData();
                body.append('mail', params.mail);
                body.append('password', params.password);
                body.append("push_id", push_id);
                //let data = await this.restangular.all(url).customPOST(body).toPromise();
                let data = await this.http.post(this.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                resolve(data);
            } catch (err) {
                console.log("Error : " , err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }
    
    
    // Get all categories ;
    async RegisterUser(url,params,push_id)
    {
        return new Promise(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            
            try {
                let body = new FormData();
                body.append('name', params.name);
                body.append('mail', params.mail);
                body.append('address', params.address);
                body.append('phone', params.phone);
                body.append('newsletter','1');
                body.append("push_id", push_id);
                body.append('mail', params.mail);
                body.append('password', params.password);
               // let data = await this.restangular.all(url).customPOST(body).toPromise();
                let data = await this.http.post(this.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                console.log("Reg : " , data)
                resolve(data);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }


    SetUserPush(url,push_token:any,user_id)
    {
        if (push_token)
            localStorage.setItem("push_id", push_token);

            try {
                let body = new FormData();
                body.append("user_id", localStorage.getItem("userid"));
                body.append("push_id", localStorage.getItem("push_id"));
                return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data) => {
                }).toPromise();
            } catch (err) {
                console.log( err);
            } finally {
            }
    }


    disconnectPush(url,user_id)
    {
            try {
                let body = new FormData();
                body.append("user_id", user_id);
                return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data) => {
                }).toPromise();
            } catch (err) {
                console.log( err);
            } finally {
            }
        }
}
