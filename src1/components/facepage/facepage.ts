import { Component } from '@angular/core';

/**
 * Generated class for the FacepageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'facepage',
  templateUrl: 'facepage.html'
})
export class FacepageComponent {

  text: string;

  constructor() {
    console.log('Hello FacepageComponent Component');
    this.text = 'Hello World';
  }

}
