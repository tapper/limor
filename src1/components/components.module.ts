import { NgModule } from '@angular/core';
import { HeaderCompComponent } from './header-comp/header-comp';
import { GoToBasketPopupComponent } from './go-to-basket-popup/go-to-basket-popup';
import {IonicPageModule} from "ionic-angular";
import { DeletePopupComponent } from './delete-popup/delete-popup';
import { ProductsComponent } from './products/products';
import { FooterComponent } from './footer/footer';
import { FacebookconnectComponent } from './facebookconnect/facebookconnect';
import { FacepageComponent } from './facepage/facepage';
@NgModule({
	declarations: [HeaderCompComponent,
    GoToBasketPopupComponent,
    DeletePopupComponent,
    ProductsComponent,
    FooterComponent,
    FacebookconnectComponent,
    FacepageComponent],
	imports:[IonicPageModule.forChild([
            HeaderCompComponent,
            GoToBasketPopupComponent,
        DeletePopupComponent,
            ]),],
	exports: [HeaderCompComponent,
    GoToBasketPopupComponent,
    DeletePopupComponent,
    ProductsComponent,
    FooterComponent,
    FacebookconnectComponent,
    FacepageComponent]
})
export class ComponentsModule {}
