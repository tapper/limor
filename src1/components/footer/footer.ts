import {Component, Input, ViewChild} from '@angular/core';
import {AlertController, NavController, ViewController} from 'ionic-angular';
import {ChatPage} from "../../pages/chat/chat";
import {ConfigApp} from "../../providers/config/config.app";
import {ChatmessagesPage} from "../../pages/chatmessages/chatmessages";

/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'footer',
  templateUrl: 'footer.html'
})
export class FooterComponent {

    public PageName: any = '';
    @ViewChild('myNav') nav: NavController;
    @Input() ProductId;

    showChatTab: boolean = false;
    isAdmin : any = localStorage.getItem('is_Admin');
    userId : any = localStorage.getItem('userid');
    public AppSettings : any = [];



    constructor(public navCtrl: NavController,public viewCtrl: ViewController,private alertCtrl: AlertController,public config: ConfigApp) {
    console.log('Hello FooterComponent Component');
    this.PageName = viewCtrl.name;
    this.AppSettings = this.config.AppSettings;


    if (this.PageName == "ProductPage" && this.isAdmin == 0) {
        this.showChatTab = true;
    }
  }

    goHomePage() {
        this.navCtrl.setRoot('HomePage');
    }

    goProductChatPage() {
        if (this.userId != "" && this.userId != undefined && this.userId != null) {
            if (this.isAdmin == 1) {
                this.navCtrl.push(ChatmessagesPage);
            } else {
                this.navCtrl.push(ChatPage, { product_id: 0});
            }
        }
        else
            this.navCtrl.push('LoginPage');
    }


    goProfilePage() {
        if (this.userId != "" && this.userId != undefined && this.userId != null)
        this.navCtrl.push('ProfilePage');
      else
          this.navCtrl.push('LoginPage');
    }

    gobasketPage() {
        this.navCtrl.push('BasketPage');

    }

}
