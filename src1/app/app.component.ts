import {Component, ViewChild} from '@angular/core';
import {AlertController, Events, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {ApiProvider} from "../providers/api/api";
import {ProductsPage} from "../pages/products/products";
import {BasketProvider} from "../providers/api/api.basket";
import {Storage} from '@ionic/storage';
import {Firebase} from "@ionic-native/firebase";
import {AuthProvider} from "../providers/auth/auth";
import {ChatSevice} from "../providers/api/chat";
import { ChatPage } from '../pages/chat/chat';
import {ChatmessagesPage} from "../pages/chatmessages/chatmessages";
import {ConfigApp} from "../providers/config/config.app";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    
    //rootPage: any = 'HomePage'; //BasketPage; //
    
    pages: Array<{ title: string, component: any }>;

    Obj: any;
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public newdate: any;
    
    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public api: ApiProvider,
                public splashScreen: SplashScreen,
                public storage:Storage,
                public basketProvider: BasketProvider,
                public alertCtrl: AlertController, private firebase: Firebase,public auth:AuthProvider, public events: Events, public ChatService: ChatSevice,public config:ConfigApp,
    ) {
        
        this.initializeApp();
        
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: 'HomePage' },
            { title: 'LogOut', component: 'LogOut' }
        ];

        events.subscribe('userConnected', (is_Admin) => {
           if (is_Admin == "1") {
               let pageFound = 0;
               for(let i=0; i < this.pages.length; i++) {
                   if (this.pages[i].component == 'ChatmessagesPage')
                       pageFound = 1;
               }
               if (pageFound == 0)
                   this.pages.splice(1, 0, { title: 'Chat Messages', component: 'ChatmessagesPage' });
           }
           else {
               for(let i=0; i < this.pages.length; i++) {
                   if (this.pages[i].component == 'ChatmessagesPage') {
                       this.pages.splice(i, 1);
                   }
               }
           }
        });
    }
    
    async initializeApp() {
        //   this.platform.ready().then(() => {
        //   // Okay, so the platform is ready and our plugins are available.
        //   // Here you can do any higher level native things you might need.
        //   this.statusBar.styleDefault();
        //   this.splashScreen.hide();
        // });
        
        await this.platform.ready();
        this.statusBar.styleDefault();


        if (this.platform.is('cordova')) {
            if (this.platform.is('android')) {
                this.initializeFireBaseAndroid();
            }
            if (this.platform.is('ios')) {
                this.initializeFireBaseIos();
            }
        }
        
        
        this.platform.registerBackButtonAction(() => {
            
            if (this.nav.length() == 1) {
                
                let alert = this.alertCtrl.create({
                    title: 'Exit',
                    message: 'Do you want to exit?',
                    buttons: [
                        {
                            text: "OK", handler: () => {
                            this.platform.exitApp();
                        }
                        },
                        {text: "Cancel", role: 'cancel'}
                    ]
                });
                alert.present();
                
            } else
                this.nav.pop();
            
        });
        
       
        let id = localStorage.getItem('userid');
        
        //if (id != "" && id != undefined && id != null) {
            await this.api.getCategories("GetCategories");
            //console.log("MainPageProducts");
            let MainPageProducts = await this.api.getMainPageProducts("getMainPageProducts");


            await this.api.getAppSettings('getAppSettings').then((data: any) => {
                this.config.AppSettings = data.json();
                this.config.AppTitle = this.config.AppSettings.admin_title;
                console.log("AppSettings",this.config.AppSettings)
            });



            let basket = await this.storage.get('basket')
            if (basket) {
                basket = JSON.parse(basket);
                this.basketProvider.addLocalStorageToBasket(basket);
            }
            await this.nav.setRoot('HomePage'); // BasketPage
       // }
       // else {
      //      console.log("MyId1 : ", id);
      //      await this.nav.setRoot('RegisterPage'); // LoginPage
      //   }
        
    }


    private initializeFireBaseAndroid(): Promise<any> {
        return this.firebase.getToken()
            .catch(error =>
                console.error('Error getting token', error)
            )
            .then(token => {
                this.firebase.subscribe('all').then((result) => {
                    if (result) console.log(`Subscribed to all`);
                    this.subscribeToPushNotificationEvents();
                });
            });
    }

    private initializeFireBaseIos(): Promise<any> {
        return this.firebase.grantPermission()
            .catch(error => console.error('Error getting permission', error))
            .then(() => {
                this.firebase.getToken()
                    .catch(error => console.error('Error getting token', error))
                    .then(token => {
                        console.log(`The token is ${token}`);
                        this.firebase.subscribe('all').then((result) => {
                            if (result) console.log(`Subscribed to all`);
                            this.subscribeToPushNotificationEvents();
                        });
                    });
            })

    }

    sendPushtoserver(token) {
        this.config.push_id = token;
        this.auth.SetUserPush('SetUserPush',token,localStorage.getItem('userid'));
    }


    private saveToken(token: any): Promise<any> {
        // Send the token to the server
        // console.log('Sending token to the server...');
        this.sendPushtoserver(token);
        return Promise.resolve(true);
    }

    private subscribeToPushNotificationEvents(): void {

        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(
            token => {
                this.sendPushtoserver(token);
                //this.saveToken(token);
                //this.loginService.sendToken('GetToken', token).then((data: any) => {
                //    console.log("UserDetails : ", data);
                //});
            },
            error => {
                console.error('Error refreshing token', error);
            });

        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(
            (notification) => {
                // !notification.tap
                //     ? alert('The user was using the app when the notification arrived...')
                //     : alert('The app was closed when the notification arrived...');
                if (!notification.tap) {
                    //alert(JSON.stringify(notification));
                    this.date = new Date()
                    this.hours = this.date.getHours()
                    this.minutes = this.date.getMinutes()
                    this.seconds = this.date.getSeconds()

                    if (this.hours < 10)
                        this.hours = "0" + this.hours

                    if (this.minutes < 10)
                        this.minutes = "0" + this.minutes
                    this.time = this.hours + ':' + this.minutes;


                    this.today = new Date();
                    this.dd = this.today.getDate();
                    this.mm = this.today.getMonth() + 1; //January is 0!
                    this.yyyy = this.today.getFullYear();

                    if (this.dd < 10) {
                        this.dd = '0' + this.dd
                    }

                    if (this.mm < 10) {
                        this.mm = '0' + this.mm
                    }

                    this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
                    this.newdate = this.today + ' ' + this.time;


                    //alert("Notification " + notification.message );
                    this.Obj = {
                        title: notification.message,
                        date: this.newdate,
                        //type: notification.type,
                        username: notification.fullname,
                        is_Admin: notification.is_Admin,
                        //image: notification.image
                    };
                    this.ChatService.pushToArray(this.Obj);
                    this.events.publish('newchat', "newchat");
                    this.events.publish('refreshAdminMessages', "refreshAdminMessages");

                }
                else {

                    setTimeout( () =>
                    {
                            //alert(JSON.stringify(notification));
                            if (localStorage.getItem('userid')) {
                                if (localStorage.getItem('is_Admin') == "0") {
                                    this.nav.push(ChatPage);
                                    //this.nav.push('ChatPage', { product_id: notification.product_id});
                                }
                                else {
                                    this.nav.push(ChatmessagesPage);
                                }
                            }
                        }, 3000);
                    }
            },
            error => {
                console.error('Error getting the notification', error);
            });
    }
    
    openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
        
        if(page.component != 'LogOut')
            this.nav.push(page.component);
        else
        {
            this.auth.disconnectPush("disconnectPush",localStorage.getItem('userid'));
            this.storage.clear();
            localStorage.clear();
            this.nav.setRoot('HomePage');
        }
    }
}
