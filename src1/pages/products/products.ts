import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ConfigApp} from "../../providers/config/config.app";
import {ProductPage} from "../product/product";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-products',
    templateUrl: 'products.html',
})

export class ProductsPage{
    
    public subCatgories: any[] = this.api.subCategories;
    public products: any[] = this.api.products;
    public Host:String = this.config.ServerImageHost;
    public subCategoryName = "";
    
    public SubId: Number;
    
    constructor(private ga: GoogleAnalytics  , public navCtrl: NavController,public zone: NgZone, public api: ApiProvider,public config:ConfigApp, public navParams: NavParams) {
        console.log(this.subCatgories)
        this.subCategoryName = this.subCatgories[0].title;
        console.log("subCategoryName : " , this.subCategoryName)
        //suscribe for product
        this.api._products.subscribe(val => {
            this.zone.run(() => {
                this.products = val;
                console.log("PRODUCTS : " , this.products)
            });
        });

        this.ga.startTrackerWithId('UA-132063366-1')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('category page : '+ this.subCategoryName);
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }
    
    async ionViewDidLoad() {
    
    }
    
    async subCategoryClick(id,i)
    {
        let data = await this.api.getProducts("getProducts",id);
        console.log("MyData : " , data);
        this.subCategoryName = this.subCatgories[i].title;
        console.log("ProductsPage : " ,this.products);
    }
    
}


