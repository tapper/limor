import {Component, NgZone, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {ConfigApp} from "../../providers/config/config.app";
import {ChatSevice} from "../../providers/api/chat";
import {ChatPage} from "../chat/chat";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";


@IonicPage()
@Component({
  selector: 'page-chatmessages',
  templateUrl: 'chatmessages.html',
})
export class ChatmessagesPage implements OnInit {

    public ServerImageHost:any;
  public MessagesArray:any = [];
    public _MessagesArray = new Subject<any>();
    MessagesArray$: Observable<any> = this._MessagesArray.asObservable();

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public ChatService: ChatSevice, public zone: NgZone,public config:ConfigApp) {

      this.ServerImageHost = this.config.ServerImageHost;
      document.addEventListener('resume', () => {
          this.getAdminMessages();
      });

      events.subscribe('refreshAdminMessages', (user, time) => {
          this.getAdminMessages();
      });


      this._MessagesArray.subscribe(val => {
          this.zone.run(() => {
              this.MessagesArray = val;
          });
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatmessagesPage');
  }

    getAdminMessages() {
    this.MessagesArray = [];
        this.ChatService.getAdminMessages('getAdminMessages').then((data: any) => {
          this.MessagesArray = data;
            this._MessagesArray.next(this.MessagesArray);
            console.log("getAdminMessages: ", data);
        });
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
     this.getAdminMessages();
    }

    goToChatPage(recipent,product_id) {
        this.navCtrl.push(ChatPage, {
            recipent: recipent,
            product_id: product_id
        });
    }

}
