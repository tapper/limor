import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {BasketPage} from "../basket/basket";
import {RegisterPage} from "../register/register";
import {AuthProvider} from "../../providers/auth/auth";
import {PopupsProvider} from "../../providers/popups/popups";
import {Storage} from '@ionic/storage';
import {ApiProvider} from "../../providers/api/api";
import {ConfigApp} from "../../providers/config/config.app";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    
    public Login =
        {
            "mail" : "",
            "password" : ""
            
        }
    public emailregex;
    public loginStatus;
    public serverResponse;
    
    constructor(public navCtrl: NavController,
                public Popup:PopupsProvider ,
                public auth:AuthProvider,
                private ga: GoogleAnalytics ,
                public navParams: NavParams,
                public alertCtrl:AlertController,
                public storage: Storage,
                public api: ApiProvider,public events: Events,public config:ConfigApp) {
                    this.ga.startTrackerWithId('UA-132063366-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackView('Start Login  ');
                    })
                    .catch(e => console.log('Error starting GoogleAnalytics', e));
    }
    
    async loginUser()
    {
        this.emailregex = /\S+@\S+\.\S+/;
        console.log("Login1");
        if (this.Login.mail =="")
            this.Popup.presentAlert("Mail Error",'Please enter mail','');
        else if (!this.emailregex.test(this.Login.mail)) {
            this.Popup.presentAlert("Mail Error",'Please Enter correct Mail','');
            this.Login.mail= '';
            console.log("Login2");
        }
        else if (this.Login.password =="")
        {
            console.log("Login3");
            this.Popup.presentAlert("Password Error",'Please enter password','');
        }
        else
        {
            console.log("Login4 " , this.Login);
            let data = await this.auth.LoginUser("UserLogin",this.Login,this.config.push_id);


                console.log("login response: ",  data);
                if (data == 0) {
                    this.Popup.presentAlert("Password Error",'Your email and password not match please try again','');
                    this.Login.password = '';
                }
                else{
                    this.storage.set('userid' , data[0].id);
                    this.storage.set('name' , data[0].name);
                    this.storage.set('is_Admin' , data[0].is_Admin);

                    localStorage.setItem('userid', data[0].id);
                    localStorage.setItem('name', data[0].name);
                    localStorage.setItem('is_Admin', data[0].is_Admin);

                    this.events.publish('userConnected',data[0].is_Admin);
                    this.Login.mail= '';
                    this.Login.password= '';
                    await this.api.getCategories("GetCategories");

                    this.ga.startTrackerWithId('UA-132063366-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackView('End Login  ');
                    })
                    .catch(e => console.log('Error starting GoogleAnalytics', e));

                    this.navCtrl.setRoot('HomePage');
                }
        }
        
    }
    
    goRegisterPage()
    {
        this.navCtrl.setRoot('RegisterPage');
    }
    
    goForgotPage()
    {
        //this.navCtrl.push(ForgotPage);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }
    
    
}
