import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {BasketPage} from "../basket/basket";
import {HomePage} from "../home/home";
import {PopupsProvider} from "../../providers/popups/popups";
import {AuthProvider} from "../../providers/auth/auth";
import {ApiProvider} from "../../providers/api/api";
import {Storage} from '@ionic/storage';
import {ConfigApp} from "../../providers/config/config.app";
import { GoogleAnalytics } from "@ionic-native/google-analytics";


/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})

export class RegisterPage implements OnInit{
    
    public products;
    public isAvaileble = false;
    public TotalPrice = 0;
    public ServerHost;
    public emailregex;
    public serverResponse;
    
    public Details = {
        'name':'',
        'mail':'',
        'password':'',
        'address':'',
        'phone':'',
        'newsletter' : false
    }
    
    
    constructor(private ga: GoogleAnalytics ,
                public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl:AlertController,
                public modalCtrl: ModalController,
                public Popup:PopupsProvider,
                public auth:AuthProvider,
                public api: ApiProvider,
                public storage: Storage,public config:ConfigApp
                ) {
        console.log("Reg")

        this.ga.startTrackerWithId('UA-132063366-1')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('Start register : ');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }
    
    ionViewDidLoad() {
    
    }
    
    ngOnInit()
    {
    
    }
    
    gotoLogIn()
    {
        console.log("tt23")
        this.navCtrl.push('LoginPage');
    }
    
    
    async doRegister()
    {
        this.emailregex = /\S+@\S+\.\S+/;
        if(this.Details.name.length < 3)
            this.Popup.presentAlert("Name Error",'Please enter name','');
        
        else if(this.Details.address.length < 3)
            this.Popup.presentAlert("Address Error",'Please enter address','');
        
        else if(this.Details.mail =="")
            this.Popup.presentAlert("Mail Error",'Please enter mail','');
        
        else if (!this.emailregex.test(this.Details.mail)) {
            this.Popup.presentAlert("Mail Error",'Please Enter correct Mail','');
            this.Details.mail = '';
        }
        
        else if(this.Details.password =="")
            this.Popup.presentAlert("Password Error",'Please enter password','');
        
        else
        {
            let data = await this.auth.RegisterUser("RegisterUser",this.Details,this.config.push_id);
            
                console.log("register response: ",  data);
                if(data != 0)
                {
                    this.ga.startTrackerWithId('UA-132063366-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackView('End Register ');
                    })
                    .catch(e => console.log('Error starting GoogleAnalytics', e));
                        this.storage.set('userid' , data[0].id);
                        this.storage.set('name' , data[0].name);
                        this.storage.set('is_Admin' , 0);

                        localStorage.setItem('userid', data[0].id);
                        localStorage.setItem('name', data[0].name);
                        localStorage.setItem('is_Admin', '0');

                        this.Details.name = '';
                        this.Details.mail = '';
                        this.Details.address = '';
                        this.Details.phone = '';
                        this.Details.newsletter = false;
                        await this.api.getCategories("GetCategories");
                        this.navCtrl.push('HomePage');
                }
               else
                {
                    this.Popup.presentAlert("Details Error",'This mail already exists , please enter another mail','');
                }
           
        }
    }
}
