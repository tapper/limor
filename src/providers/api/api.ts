
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Product} from './api.product';
import {LoadingController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Http} from "@angular/http";
import {Restangular} from 'ngx-restangular';
import {ConfigApp} from "../config/config.app";


@Injectable()
export class ApiProvider {
    
    public Analitics = "UA-132054452-1"
    public mixPanel = "fdf0e2d60bae784a2d86a68d0a98968e"; //"521344fa82068abbf994eb336cb5d507" "2da84795c5f7e890fd843a70cb7a475f";
    private _categories: BehaviorSubject<any> = new BehaviorSubject(null);
    readonly categories$: Observable<any> = this._categories.asObservable();
    get categories(): Array<any> {return this._categories.getValue();}
    
    private _subCategories: BehaviorSubject<any> = new BehaviorSubject(null);
    readonly subCategories$: Observable<any> = this._subCategories.asObservable();
    get subCategories(): Array<any> {return this._subCategories.getValue();}
    
    public _products: BehaviorSubject<Array<Product>> = new BehaviorSubject(null);
    readonly products$: Observable<Array<Product>> = this._products.asObservable();
    get products() : Array<Product> {return this._products.getValue();}
    
    public _MainPageproducts: BehaviorSubject<Array<Product>> = new BehaviorSubject(null);
    readonly MainPageproducts$: Observable<Array<Product>> = this._MainPageproducts.asObservable();
    get MainPageproducts() : Array<Product> {return this._MainPageproducts.getValue();}
    
    
    constructor(public loadingCtrl: LoadingController,
                public storage: Storage,
                private http:Http,
                private config:ConfigApp,
                public restangular: Restangular) {}
    
    
    
    // Get all categories ;
    async getCategories(url): Promise<Array<Product>> {
        return new Promise<Array<Product>>(async (resolve, reject) => {
            //let loading = this.loadingCtrl.create({content: 'Please wait...'});
            //loading.present();
            
            try {
                let body = new FormData();
                body.append('id', '0');
                let categories = await this.restangular.all(url).customPOST(body).toPromise();

                //let categories = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                this._categories.next(categories);
                resolve(this.categories);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                //loading.dismiss();
            }
        });
    }
    
    // Get all subCategories ;
    async getSubCategories(url,id): Promise<Array<Product>> {
        return new Promise<Array<Product>>(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            
            try {
                let body = new FormData();
                body.append('id', id);
                let subCategories = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                this._subCategories.next(subCategories);
                resolve(this.subCategories);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }
    
    
    // Get all products ;
    async getProducts(url,id): Promise<Array<Product>> {
        return new Promise<Array<Product>>(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            try {
                let body = new FormData();
                body.append('id', id);
                let products = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                console.log("TP : " ,products ,url)
                this._products.next(products);
                resolve(products);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }
    
    //getMainPageProducts
    async getMainPageProducts(url): Promise<Array<Product>> {
        return new Promise<Array<Product>>(async (resolve, reject) => {
            //let loading = this.loadingCtrl.create({content: 'Please wait...'});
            //loading.present();
            try {
                console.log("MainPageProductsService ",url);
                let body = new FormData();
                let MainPageproducts = await this.http.post(this.config.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                console.log("MainPageProducts " , MainPageproducts,url)
                this._MainPageproducts.next(MainPageproducts);
                resolve(MainPageproducts);
            } catch (err) {
                let MainPageproducts = [];
                this._MainPageproducts.next(MainPageproducts);
                resolve(MainPageproducts);
                console.log(err , MainPageproducts);
                reject(err);
            } finally {
                //loading.dismiss();
            }
        });
    }


    getCampignImages(url) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            let body = new FormData();
            //body.append("category", JSON.stringify(data));
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            //loading.dismiss();
        }
    }


    getUserProfile(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }


    UpdateUserProfile(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            body.append("category", JSON.stringify(data));
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }


    getSubCategoryProducts(url,prd_id,subcat_id) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            let body = new FormData();
            body.append("prd_id", prd_id);
            body.append("subcat_id", subcat_id);
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            //loading.dismiss();
        }
    }

    getAppSettings(url) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            let body = new FormData();
            //body.append("category", JSON.stringify(data));
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            //loading.dismiss();
        }
    }

    faceBookLogin(url,data) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", data);
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    getHistory(url) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        let userId = "";
        try{
            userId = localStorage.userid;
        }catch(err){}

        try {
            let body = new FormData();
            body.append("userid", userId);
            return this.http.post(this.config.ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }
}


//
// com.tapper.ytravel
// SKU A unique ID for your app that is not visible on the App Store.
//     YTravel001
// Apple ID An automatically generated ID assigned to your app.
// 1362024077

