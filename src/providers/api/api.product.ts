export type Product = {
    id: number;
    category_id: number;
    sub_category_id: number;
    title:string,
    low_price:number,
    high_price:number,
    description:string,
    image:string,
    details_image:string,
    date: string;
}