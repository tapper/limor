import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable, ViewChild} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {ConfigApp} from "../config/config.app";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {Content} from "ionic-angular";
import {Storage} from '@ionic/storage';


@Injectable()

export class ChatSevice
{

    @ViewChild(Content) content: Content;
    constructor(private http:Http,public Settings:ConfigApp,public storage: Storage) {
        this.ServerUrl = this.Settings.ServerUrl;
    };

    ChatArray:any[] = [];
    public _ChatArray = new Subject<any>();
    ChatArray$: Observable<any> = this._ChatArray.asObservable();
    public ServerUrl;


    addTitle(url:string , title:string,datetime:string,time:string,name:string,image:string,chatType:string,
     product_id,is_Admin,recipent)

    {
        let body = new FormData();
        body.append('uid', window.localStorage.userid );
        body.append('title', title);
        body.append('type', chatType);
        body.append('product_id', product_id );
        body.append('is_Admin', is_Admin );
        body.append('recipent', recipent );
        body.append('date', datetime );
        body.append('time', time );
        body.append('name', name );
        //body.append('image', image );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("Chat : " , data)}).toPromise();
    }

    getChatDetails(url:string,user_id:string,product_id:number)
    {
        let body = new FormData();
        body.append('user_id', user_id);
        body.append('product_id', product_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.updateChatArray(data)}).toPromise();
    }

    getProductInfo(url:string,product_id:number)
    {
        let body = new FormData();
        body.append('product_id', product_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    updateChatArray(data)
    {
        this.ChatArray = data;
        this._ChatArray.next({ text: this.ChatArray });
    }

    pushToArray(data)
    {
        //this.ChatArray.push(data);
        this.ChatArray.splice((this.ChatArray.length),0,data);
        this._ChatArray.next(this.ChatArray);
    }

    registerPush(url,pushid)
    {
        let body = new FormData();
        //body.append('push_id', pushid.toString() );
        //body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("chat:" , data)});
    }

    getChatMessagesCount(url)
    {
        let body = new FormData();
       // body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }

    getAdminMessages(url)
    {
        let body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }
};


