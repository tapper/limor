import { Injectable } from '@angular/core';
import {AlertController} from "ionic-angular";

/*
  Generated class for the PopupsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PopupsProvider {

  constructor(public alertCtrl:AlertController) {
    console.log('Hello PopupsProvider Provider');
  }
    
    presentAlert(Title , Message,PageRedirect, fun:any = null) {
        let alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                text: 'סגור',
                handler: () => {if(PageRedirect == 1) {fun; }}
            }],
            cssClass: 'alertRtl'
        });
        alert.present();
    }

}
