import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AlertController, IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';


import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ApiProvider} from "../providers/api/api";
import {IonicStorageModule} from "@ionic/storage";
import {HttpModule} from "@angular/http";
import {ConfigApp} from "../providers/config/config.app";
import {BasketProvider} from "../providers/api/api.basket";
import {ChatSevice} from "../providers/api/chat";

import {ComponentsModule} from "../components/components.module";
import {RestangularModule} from 'ngx-restangular';
import { AuthProvider } from '../providers/auth/auth';
import { PopupsProvider } from '../providers/popups/popups';
import {Firebase} from "@ionic-native/firebase";
import { ChatPage } from '../pages/chat/chat';
import { ChatmessagesPage } from '../pages/chatmessages/chatmessages';
import { ChatmodalPage } from '../pages/chatmodal/chatmodal';
import { Facebook } from '@ionic-native/facebook';


import { AutosizeDirective } from '../directives/autosize/autosize';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { ZoomAreaModule } from 'ionic2-zoom-area';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Mixpanel } from "@ionic-native/mixpanel";



export function RestangularConfigFactory(RestangularProvider) {

    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl("http://limor.tapper.org.il/laravel/public/api/");


    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data) {
            console.log(url, data, operation);
        }
        return data;
    });

    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(async (response, subject, responseHandler) => {

        console.log('ErrorInterceptor', response);

        // if (response.data && !response.success){
        //     if (response.data.error && response.data.error.message){
        //         alertCtrl.create({title: response.data.error.message, buttons: ['OK']}).present();
        //     } else {
        //         alertCtrl.create({title: 'Server error!', buttons: ['OK']}).present();
        //     }
        // }

    });

}



@NgModule({
    declarations: [
        MyApp,ChatPage,ChatmodalPage,AutosizeDirective,
        ChatmessagesPage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(),
        ComponentsModule,
        RestangularModule.forRoot([], RestangularConfigFactory),
        HttpModule,
        BrowserAnimationsModule,
        ZoomAreaModule.forRoot(),
        IonicImageViewerModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,ChatPage,ChatmodalPage,ChatmessagesPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ApiProvider,
        BasketProvider,
        ChatSevice,
        ConfigApp,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
         AuthProvider,
    PopupsProvider,
        Firebase,
        AutosizeDirective,
        FileTransfer,
        FileTransferObject,
        File,
        Camera,
        FilePath,
        Mixpanel,
    ]
})
export class AppModule {
}
