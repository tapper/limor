import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TakanonPage } from './takanon';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    TakanonPage,
  ],
  imports: [
    IonicPageModule.forChild(TakanonPage),
    ComponentsModule
  ],
})
export class TakanonPageModule {}
