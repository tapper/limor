import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormControl, FormGroup, Validators,NgForm} from "@angular/forms";
import {ConfigApp} from "../../providers/config/config.app";
import {ApiProvider} from "../../providers/api/api";
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

    public waitforsave:boolean = false;
    public UserId;


    profileFields: any =  {
        "fullname" : "",
        "address" : "",
        "phone" : ""
    }

  constructor(public navCtrl: NavController, public navParams: NavParams,public config:ConfigApp, public api: ApiProvider,private toastCtrl: ToastController) {
        this.UserId = localStorage.getItem('userid');
  }


  getUserProfile() {
      this.api.getUserProfile('getUserProfile',this.UserId).then((data: any) => {
          let response = data.json();
          this.profileFields.fullname = response.name;
          this.profileFields.address = response.address;
          this.profileFields.phone = response.phone;
          this.waitforsave = true;
      });
  }
    async ngOnInit() {
         this.getUserProfile();
    }

    onSubmit(form:NgForm) {
        console.log(form.value);
        if (this.waitforsave) {

            this.api.UpdateUserProfile('UpdateUserProfile',this.profileFields,this.UserId).then((data: any) => {
                let toast = this.toastCtrl.create({
                    message: 'Updated successfully',
                    duration: 3000,
                    position: 'bottom'
                });

                toast.present();

                this.navCtrl.setRoot('HomePage');

            });

        }

    }

    goToOrders()
    {
        this.navCtrl.push('HistoryPage');
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
