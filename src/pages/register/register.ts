import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {BasketPage} from "../basket/basket";
import {HomePage} from "../home/home";
import {PopupsProvider} from "../../providers/popups/popups";
import {AuthProvider} from "../../providers/auth/auth";
import {ApiProvider} from "../../providers/api/api";
import {Storage} from '@ionic/storage';
import {ConfigApp} from "../../providers/config/config.app";
import { Mixpanel } from "@ionic-native/mixpanel";




/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})

export class RegisterPage implements OnInit{
    
    public products;
    public isAvaileble = false;
    public TotalPrice = 0;
    public ServerHost;
    public emailregex;
    public serverResponse;
    
    public Details = {
        'name':'',
        'mail':'',
        'password':'',
        'address':'',
        'phone':'',
        'newsletter' : false
    }
    
    
    constructor(
                public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl:AlertController,
                public modalCtrl: ModalController,
                public Popup:PopupsProvider,
                public auth:AuthProvider,
                public api: ApiProvider,
                private mixpanel: Mixpanel,
                public storage: Storage,public config:ConfigApp
                ) {
        console.log("Reg")

        mixpanel.init(api.mixPanel).then(() => {
            console.log("Mix : " )
           mixpanel.track("RegisterPage : " , { })
        }).catch(e => console.log('Error MixPanel', e));
    }
    
    ionViewDidLoad() {
    
    }
    
    ngOnInit()
    {
    
    }
    
    gotoLogIn()
    {
        console.log("tt23")
        this.navCtrl.push('LoginPage');
    }
    
    
    async doRegister()
    {
        this.emailregex = /\S+@\S+\.\S+/;
        if(this.Details.name.length < 3)
            this.Popup.presentAlert("Name Error",'Please enter name','');
        
        else if(this.Details.address.length < 3)
            this.Popup.presentAlert("Address Error",'Please enter address','');
        
        else if(this.Details.mail =="")
            this.Popup.presentAlert("Name Error",'Please enter name','');
        
        else if (this.Details.mail.length < 3) {
            this.Popup.presentAlert("Name Error",'Please Enter correct name','');
            this.Details.mail = '';
        }
        
        else if(this.Details.password =="")
            this.Popup.presentAlert("Password Error",'Please enter password','');
        
        else
        {
            let data = await this.auth.RegisterUser("RegisterUser",this.Details,this.config.push_id);
            
                console.log("register response: ",  data);
                if(data != 0)
                {   
                  
                    this.mixpanel.init(this.api.mixPanel).then(() => {
                        console.log("Mix : " )
                        this.mixpanel.track("AfterRegisterInPage : " + data[0].name, { }).catch(e => console.log('Error MixPanel', e));; 
                    }).catch(e => console.log('Error MixPanel', e));

                        this.storage.set('userid' , data[0].id);
                        this.storage.set('name' , data[0].name);
                        this.storage.set('is_Admin' , 0);

                        localStorage.setItem('userid', data[0].id);
                        localStorage.setItem('name', data[0].name);
                        localStorage.setItem('is_Admin', '0');

                        this.Details.name = '';
                        this.Details.mail = '';
                        this.Details.address = '';
                        this.Details.phone = '';
                        this.Details.newsletter = false;
                        await this.api.getCategories("GetCategories");
                        let goToBasket = "0"
                        try{
                            goToBasket =  localStorage.basket 
                        }catch(err)
                        {

                        }

                        if(goToBasket == "1")
                        {
                            localStorage.basket = "0";
                            this.navCtrl.push('BasketPage'); 
                        }
                        else
                        this.navCtrl.push('HomePage');
                }
               else
                {
                    this.Popup.presentAlert("Details Error",'This userName already exists , please enter another mail','');
                }
           
        }
    }
}
