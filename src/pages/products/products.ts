import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ConfigApp} from "../../providers/config/config.app";
import {ProductPage} from "../product/product";
import { Mixpanel } from "@ionic-native/mixpanel";



/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-products',
    templateUrl: 'products.html',
})

export class ProductsPage{
    
    public subCatgories: any[] = this.api.subCategories;
    public products: any[] = this.api.products;
    public Host:String = this.config.ServerImageHost;
    public subCategoryName = "";
    public selectedCategory:Number = 0;
    
    public SubId: Number;
    
    constructor(public navCtrl: NavController, private mixpanel: Mixpanel, public zone: NgZone, public api: ApiProvider,public config:ConfigApp, public navParams: NavParams ,) {
        console.log(this.subCatgories)
        this.subCategoryName = this.subCatgories[0].title;
        this.selectedCategory = this.subCatgories[0]['id'];
        console.log("subCategoryName : " , this.subCategoryName , this.selectedCategory , this.subCatgories)
        //suscribe for product
        this.api._products.subscribe(val => {
            this.zone.run(() => {
                this.products = val;
                console.log("PRODUCTS : " , this.products)
            });
        });

        this.mixpanel.init(api.mixPanel).then(() => {
            console.log("Mix : " , this.subCategoryName)
            mixpanel.track("Products : ",{ 
                "SubCategory": this.subCategoryName , 
              });
           
        }).catch(e => console.log('Error MixPanel', e));
    }
    
    async ionViewDidLoad() {
    
    }
    
    async subCategoryClick()
    {
        console.log("SubClick")
        let data = await this.api.getProducts("getProducts",this.selectedCategory);
        console.log("MyData : " , data);
        let obj = this.subCatgories.findIndex(item => item['id'] == this.selectedCategory);
        this.subCategoryName = obj['title'] //this.subCatgories[i].title;
        console.log("ProductsPage : " ,this.products);
    }

    goLeft()
    {

    }
    
}


