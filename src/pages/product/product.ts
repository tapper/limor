import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {GoToBasketPopupComponent} from "../../components/go-to-basket-popup/go-to-basket-popup";
import {BasketProvider} from "../../providers/api/api.basket";
import {BasketPage} from "../basket/basket";
import {ChatPage} from "../chat/chat";
import {ConfigApp} from "../../providers/config/config.app";
import {ApiProvider} from "../../providers/api/api";
import { Mixpanel, MixpanelPeople } from '@ionic-native/mixpanel';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-product',
    templateUrl: 'product.html',
})
export class ProductPage {
    public product: any;
    public PrdQuan: number;
    public baskets:any[]=[];
    public is_Admin:any;
    public Host:String;
    public categoriesProducts: any = [];
    public productId : any = '';


    constructor(
                public navCtrl: NavController,
                public navParams: NavParams,
                public modalCtrl:ModalController,
                private mixpanel: Mixpanel,
                public basketProvider:BasketProvider ,public config:ConfigApp, public api: ApiProvider) {
        this.product = navParams.get('product');
        this.productId = this.product.id;
        this.PrdQuan = 1;
        this.is_Admin = localStorage.getItem('is_Admin');
        this.Host = this.config.ServerImageHost;
        console.log("product:",this.product)
        this.basketProvider.basket$.subscribe(basket => this.baskets = basket);
        this.getSubCategoryProducts(this.product.id,this.product.sub_category_id);
        console.log("Product Page : ")
        this.mixpanel.init(api.mixPanel).then(() => {
            console.log("Mix : " , this.product)
            mixpanel.track("Product : " , { 
                "Product Name": this.product['title'] , 
                "Category Name": this.product['CategoryName'],
                "SKU":this.product['sku'],
                "id":this.product['id'] 
              });
           
        })
        .catch(e => console.log('Error MixPanel', e));

    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad ProductPage');
    }
    
    async changeQuan(type) {
        type === 1 ? this.PrdQuan++ : this.PrdQuan--;
        if(this.PrdQuan < 1 ) this.PrdQuan = 1 ;
    }
    
    openModal()
    {
        console.log("Modal");
        let modal = this.modalCtrl.create(GoToBasketPopupComponent);
        modal.present();
    
        modal.onDidDismiss(data => {
            console.log("bs3 : " , this.product)
            if(data)
            {
                this.product.qnt = this.PrdQuan;
                this.basketProvider.pushToBasket(this.product);
                data == 1 ? this.navCtrl.pop() : this.navCtrl.push('BasketPage');
            }
        });
    }

    gotoChatPage() {
        this.navCtrl.push(ChatPage, { product_id: this.product.id});
    }


    getSubCategoryProducts(prd_id,subcat_id) {
        this.api.getSubCategoryProducts('getSubCategoryProducts',prd_id,subcat_id).then((data: any) => {
            this.categoriesProducts = data.json();
            console.log("categoriesProducts : " , this.categoriesProducts ,prd_id , subcat_id )
        });
    }
}
