import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {

    public subcat : any = '';
    public productsArray:any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
      this.subcat = navParams.get('subcat');
      this.getItems (this.subcat)
  }

  async getItems(subcat:any)
  {
      if (subcat != 0){
          let data = await this.api.getProducts("getProducts",subcat);
          this.productsArray = data;
          console.log("ProductsPage : " ,this.productsArray);
      }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }

}
