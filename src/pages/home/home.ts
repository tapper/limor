
import {Component, NgZone, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, Events} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {Product} from '../../providers/api/api.product';
import {ConfigApp} from "../../providers/config/config.app";
import {ProductsPage} from "../products/products";
import { Mixpanel } from "@ionic-native/mixpanel";
import { Platform } from 'ionic-angular';

//ionic cordova plugin add cordova-plugin-facebook4 --variable APP_ID="2244397375604440" --variable APP_NAME="Limor online"
@IonicPage()

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage  implements OnInit {
    public categoires:any[] = [];
    public Host:String;
    public MainPageproducts: any[] = this.api.MainPageproducts;
    public CampignImages:any[] = [];

    //jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore limor.keystore app-release-unsigned.apk limor
    //zipalign -v 4 app-release-unsigned.apk limorR.apk
    constructor(public platform: Platform , public navCtrl: NavController,private mixpanel: Mixpanel, public zone: NgZone, public api: ApiProvider ,public config:ConfigApp, public navParams: NavParams, public events: Events) {
        this.categoires = this.api.categories;
        this.Host = this.config.ServerImageHost;
        console.log(this.categoires );
    
        this.api._MainPageproducts.subscribe(val => {
            this.zone.run(() => {
                this.MainPageproducts = val;
                console.log("MainPage : " , this.MainPageproducts)
            });
        });

      
        this.mixpanel.init(api.mixPanel).then(() => {
            mixpanel.track("HomePage : " ,{ }); 
        }).catch(e => console.log('Error MixPanel', e));

    }

    campignpage(id)
    {
        this.navCtrl.push('CategoriesPage', {'subcat': id});
    }



    async gotoProductsPage(id)
    {
        await this.api.getSubCategories("GetSubCategoriesById",id);
        console.log("subCategories",this.api.subCategories)
        await this.api.getProducts("getProducts",this.api.subCategories[0].id);
        this.navCtrl.push('ProductsPage');
    }

    getCampignImages() {
        this.api.getCampignImages('WebgetCampiagnImages').then((data: any) => {
            this.CampignImages = data.json();
        });
    }

    ngOnInit() {
        this.events.publish('userConnected',localStorage.getItem('is_Admin'));
        this.getCampignImages();
    }
}

//UA-132063366-1