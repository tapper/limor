import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {BasketPage} from "../basket/basket";
import {RegisterPage} from "../register/register";
import {AuthProvider} from "../../providers/auth/auth";
import {PopupsProvider} from "../../providers/popups/popups";
import {Storage} from '@ionic/storage';
import {ApiProvider} from "../../providers/api/api";
import {ConfigApp} from "../../providers/config/config.app";
import { Mixpanel } from "@ionic-native/mixpanel";



/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    
    public Login =
        {
            "mail" : "",
            "password" : ""
            
        }
    public emailregex;
    public loginStatus;
    public serverResponse;
    
    constructor(public navCtrl: NavController,
                public Popup:PopupsProvider ,
                public auth:AuthProvider,
                public navParams: NavParams,
                public alertCtrl:AlertController,
                public storage: Storage,
                private mixpanel: Mixpanel,
                public api: ApiProvider,public events: Events,public config:ConfigApp) {

                    this.mixpanel.init(api.mixPanel).then(() => {
                        console.log("Mix : " )
                        this.mixpanel.track("LogInPage : ", { }); 
                    }).catch(e => console.log('Error MixPanel', e));
            
    }
    
    async loginUser()
    {
        //!this.emailregex.test(this.Login.mail)
        this.emailregex = /\S+@\S+\.\S+/;
        console.log("Login1");
        if (this.Login.mail =="")
            this.Popup.presentAlert("Name Error",'Please enter User name','');
        else if (this.Login.mail.length < 3) {
            this.Popup.presentAlert("Name Error",'Please Enter correct name','');
            this.Login.mail= '';
            console.log("Login2");
        }
        else if (this.Login.password =="")
        {
            console.log("Login3");
            this.Popup.presentAlert("Password Error",'Please enter password','');
        }
        else
        {
            console.log("Login4 " , this.Login);
            let data = await this.auth.LoginUser("UserLogin",this.Login,this.config.push_id);

                
            this.mixpanel.init(this.api.mixPanel).then(() => {
                console.log("Mix : " )
                this.mixpanel.track("AfterLogInPage : " + data[0].name, { }).catch(e => console.log('Error MixPanel', e));; 
            })

                console.log("login response: ",  data);
                if (data == 0) {
                    this.Popup.presentAlert("Password Error",'Your name and password not match please try again','');
                    this.Login.password = '';
                }
                else{
                    this.storage.set('userid' , data[0].id);
                    this.storage.set('name' , data[0].name);
                    this.storage.set('is_Admin' , data[0].is_Admin);

                    localStorage.setItem('userid', data[0].id);
                    localStorage.setItem('name', data[0].name);
                    localStorage.setItem('is_Admin', data[0].is_Admin);

                    this.events.publish('userConnected',data[0].is_Admin);
                    this.Login.mail= '';
                    this.Login.password= '';
                    await this.api.getCategories("GetCategories");

                    let goToBasket = "0"
                    try{
                        goToBasket =  localStorage.basket 
                    }catch(err)
                    {

                    }

                    if(goToBasket == "1")
                    {
                        localStorage.basket = "0";
                        this.navCtrl.push('BasketPage'); 
                    }
                    else
                    this.navCtrl.push('HomePage');
                }
        }
        
    }
    
    goRegisterPage()
    {
        this.navCtrl.setRoot('RegisterPage');
    }
    
    goForgotPage()
    {
        //this.navCtrl.push(ForgotPage);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }
    
    
}
