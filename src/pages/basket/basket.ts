import {Component, NgZone} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams,AlertController} from 'ionic-angular';
import {BasketProvider} from "../../providers/api/api.basket";
import {ConfigApp} from "../../providers/config/config.app";
import {DeletePopupComponent} from "../../components/delete-popup/delete-popup";
import {Storage} from '@ionic/storage';

import { ApiProvider } from "../../providers/api/api";
import { Mixpanel } from "@ionic-native/mixpanel";


/**
 * Generated class for the BasketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-basket',
    templateUrl: 'basket.html',
})
export class BasketPage {
    
    public basket: any[] = this.basketProvider.basket;
    public Host: any = this.config.ServerImageHost;
    public DeleteId:number;
    public PrdQuan: number;
    public TotalBasketPrice : number;
    userId = localStorage.getItem('userid');
    public AppSettings : any = [];
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public basketProvider: BasketProvider,
                public zone: NgZone,
                public config: ConfigApp,
                public modalCtrl:ModalController,
                public storage:Storage,private alertCtrl: AlertController,
                public api: ApiProvider,
                private mixpanel: Mixpanel,) {
        
        // this.basketProvider._basket.subscribe(val => {
        //     this.zone.run(() => {
        //         this.basket = val;
        //         console.log(this.basket)
        //     });
        // });

        mixpanel.init(api.mixPanel).then(() => {
            console.log("Mix : " )
           mixpanel.track("Basket : " , { }); 
        }).catch(e => console.log('Error MixPanel', e));

        this.basketProvider.basket$.subscribe(basket => this.basket = basket);
        this.basketProvider.TotalBasketPrice$.subscribe(TotalBasketPrice => this.TotalBasketPrice = TotalBasketPrice);
        this.basketProvider.calculateTotalprice();
        this.AppSettings = this.config.AppSettings;
        console.log(this.basket)

       
        
    }
    
    async ionViewDidLoad() {
        console.log('ionViewDidLoad BasketPage');
    }
    
    async changeQuan(i,type) {
        this.basketProvider.changeQnt(i,type);
    }
    
    async openDeleteModal(i)
    {
        this.DeleteId = i;
        let modal = this.modalCtrl.create(DeletePopupComponent);
        modal.present();
        
        modal.onDidDismiss(data => {
            if(data == 1)
            {
                this.basketProvider.deleteProduct(this.DeleteId);
            }
        });
    }
    
    async sendBasket()
    {
        if (this.basket.length == 0) {
            let basketEmpty = this.alertCtrl.create({
                title: 'Your shopping cart is empty',
                buttons: ['OK']
            });
            basketEmpty.present();
        }
        else {
            if (this.userId != "" && this.userId != undefined && this.userId != null)
            {
                let newAlert = this.alertCtrl.create({
                    title: this.AppSettings.basket_order_text,
                    //subTitle: '',
                    buttons: [
                        {
                            text: 'OK',
                            handler: () => {
                               

                                this.basketProvider.sendBasket('getBasket');
                                this.basketProvider.clearBasket();
                                this.storage.set('basket','');
                                this.navCtrl.setRoot('HomePage');
                            }
                        }
                    ]
                });
                newAlert.present();
            }
            else {
                localStorage.basket = "1";
                this.navCtrl.push('RegisterPage');
            }
        }
    }
}
