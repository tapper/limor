import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  public Orders:Array<object> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams , public api: ApiProvider) {
      this.getHistory()
  }

  async getHistory()
  {
    this.Orders = await this.api.getHistory("getHistory");
    console.log(this.Orders);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

}
