import {Component, Input} from '@angular/core';
import {NavController, ViewController} from "ionic-angular";
import {BasketProvider} from "../../providers/api/api.basket";
import {ConfigApp} from "../../providers/config/config.app";

/**
 * Generated class for the HeaderCompComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header-comp',
  templateUrl: 'header-comp.html'
})
export class HeaderCompComponent {

  text: string;
  public baskets:any[]=[];
  @Input() name: string;
  public AppTitle:any = '';
  
  constructor(private viewCtrl: ViewController,
              public basketProvider:BasketProvider,
              public navCtrl: NavController,public config:ConfigApp) {

      this.basketProvider.basket$.subscribe(basket => this.baskets = basket);
      console.log(this.baskets );
      this.AppTitle = this.config.AppTitle;
  }

    ionViewWillEnter() {
        this.viewCtrl.showBackButton(false);
    }

    gotoBasket()
    {
        console.log("bs")
        this.navCtrl.push('BasketPage');
    }

}
