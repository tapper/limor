import {Component, Input} from '@angular/core';
import {NavController} from "ionic-angular";

/**
 * Generated class for the ProductsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'products',
    templateUrl: 'products.html'
})
export class ProductsComponent {
    
    text: string;
    @Input() productsArray: any[];
    
    constructor(public navCtrl: NavController) {
    
    }
    
    async gotoProductPage(id) {
        console.log("gotoProductPage : ", id);
        let product = this.productsArray[id];
        console.log("gotoProductPage1 : ", product , this.productsArray);
        this.navCtrl.push('ProductPage', {'product': product});
    }
    
}
