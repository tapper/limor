import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the DeletePopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'delete-popup',
  templateUrl: 'delete-popup.html'
})
export class DeletePopupComponent {

  text: string;

  constructor(public viewCtrl: ViewController) {
  
  }
    
    onSubmit(type)
    {
        this.viewCtrl.dismiss(type);
    }

}
