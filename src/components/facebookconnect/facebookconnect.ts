import {Component, OnInit} from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import {NavController} from "ionic-angular";
import {RegisterPage} from "../../pages/register/register";
import { ApiProvider } from "../../providers/api/api";
import { HomePage } from "../../pages/home/home";

/**
 * Generated class for the FacebookconnectComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'facebookconnect',
  templateUrl: 'facebookconnect.html'
})
export class FacebookconnectComponent {

  isLoggedIn: boolean = false;
    userData: any;

    constructor(public navCtrl: NavController, private fb: Facebook, public auth: ApiProvider) {

    }




    faceBookLogin() {

        this.fb.login(['public_profile', 'email'])
            .then(res => {
                if(res.status === "connected") {
                    this.isLoggedIn = true;
                    this.getUserDetail(res.authResponse.userID);
                } else {
                    this.isLoggedIn = false;
                    //alert(JSON.stringify(res));
                }
            })
            .catch(e =>
                console.log(e)
            );
    }


    getUserDetail(userid)
    {
        this.fb.api("/"+userid+"/?fields=id,email,name,gender",["public_profile"])
            .then(res => {
                console.log(res);
                alert(JSON.stringify(res));
                alert (res['email']);
                this.auth.faceBookLogin('faceBookLogin',res['email']).then((data: any) => {
                    console.log("faceBookLogin : " , data);
                    let response = data.json();
                    //alert(JSON.stringify(response));
                    if (response.status == 0) {
                        alert("משתמש לא נמצא יש תחילה להרשם");
                        this.navCtrl.push("RegisterPage");
                    }
                    else
                    {
                        localStorage.setItem("userid", response.user_id);
                        localStorage.setItem("userType", response.userType);
                        this.navCtrl.setRoot(HomePage);
                    }
                });
            })
            .catch(e => {
                console.log(e);
            });
    }
}
